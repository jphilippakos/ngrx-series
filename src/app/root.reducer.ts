import { subModuleReducer } from './sub-module/sub-module.reducer';
import { routerReducer } from '@ngrx/router-store';
import { IAppState } from './app.interfaces';
import { BaseAction } from './base.actions';

export const rootReducer = { 
	subModule: subModuleReducer,
	router: routerReducer
};

export function clearReducer(reducer) {
	return function(state: IAppState, action: BaseAction<any>) {
		if ((action.type) === 'LOGOUT') {
			state = undefined;
		}

		return reducer(state, action);
	}
}