import { ISubState } from './sub-module/sub-module.interfaces';

export interface IAppState {
	subModule: ISubState
}