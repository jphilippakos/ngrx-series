import { BaseAction } from '../base.actions';

export enum SubModuleTypes {
	SetPropA = '[SUB MODULE] SET PROP A',
	SetPropB = '[SUB MODULE] SET PROP B',
	IncrementPropB = '[SUB MODULE] INCREMENT PROP B',
	SetRequest = '[SUB MODULE] SET REQUEST',
	SetData = '[SUB MODULE] SET DATA'
}

export class SetPropAAction extends BaseAction<string> {
	public type = SubModuleTypes.SetPropA
}

export class SetPropBAction extends BaseAction<number> {
	public type = SubModuleTypes.SetPropB
}

export class IncrementPropBAction extends BaseAction<never> {
	public type = SubModuleTypes.IncrementPropB
}

export class SetRequestAction extends BaseAction<string> {
	public type = SubModuleTypes.SetRequest
}

export class SetDataAction extends BaseAction<string> {
	public type = SubModuleTypes.SetData

	constructor(public payload: string) {
		super();
		console.log(this.payload);
	}
}