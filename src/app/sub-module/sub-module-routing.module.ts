import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubModuleLandingComponent } from './sub-module-landing/sub-module-landing.component';

const routes: Routes = [
	{ path: '', component: SubModuleLandingComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubModuleRoutingModule { }
