import { NgModule } from '@angular/core';
import { SubModuleService } from './sub-module.service';
import { SubModuleLandingComponent } from './sub-module-landing/sub-module-landing.component';
import { CommonModule } from '@angular/common';
import { SubModuleRoutingModule } from './sub-module-routing.module';
import { SubModuleEffects } from './sub-module.effects';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
	imports: [
		CommonModule,
		SubModuleRoutingModule,
		EffectsModule.forFeature([SubModuleEffects])
	],
	providers: [SubModuleService],
	declarations: [SubModuleLandingComponent]
})
export class SubModuleModule { }
