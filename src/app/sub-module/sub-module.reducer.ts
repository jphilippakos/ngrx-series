import { ISubState} from './sub-module.interfaces';
import { BaseAction } from '../base.actions';
import { SubModuleTypes } from './sub-module.actions';

export const initialState: ISubState = {
	propA: 'initial',
	propB: 13,
	request: '',
	data: null
}

export function subModuleReducer(state: ISubState = initialState, action: BaseAction<any>) {
	switch (action.type) {
		case SubModuleTypes.SetPropA:
			return { ...state, propA: action.payload };
		case SubModuleTypes.SetPropB:
			return { ...state, propB: action.payload };
		case SubModuleTypes.IncrementPropB:
			return { ...state, propB: state.propB + 1};
		case SubModuleTypes.SetRequest:
			return { ...state, request: action.payload };
		case SubModuleTypes.SetData:
			return { ...state, data: action.payload };
	}
	return state;
}