import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { SubModuleTypes, SetDataAction, SetRequestAction } from './sub-module.actions';
import { switchMap, map, tap, filter } from 'rxjs/operators';
import { SubModuleService } from './sub-module.service';

@Injectable()
export class SubModuleEffects {	
	@Effect()
	public makeApiCall$ = this._actions.pipe(
		ofType(SubModuleTypes.SetRequest),
		// filter(action => action.type === SubModuleTypes.SetRequest),
		switchMap((request: SetRequestAction) => this._service.fetchData(request.payload)),
		map(response => new SetDataAction(response))
	);

	constructor(private _actions: Actions, private _service: SubModuleService) {
		
	}
}