export interface ISubState {
	propA: string;
	propB: number;
	request: string;
	data: string;
}