import { Component } from "@angular/core";
import { SubModuleService } from '../sub-module.service';

@Component({
	selector: 'sub-module-landing',
	templateUrl: './sub-module-landing.component.html'
})
export class SubModuleLandingComponent {
	public age$ = this._service.age$;

	constructor(private _service: SubModuleService) {
	}

	public clicked() {
		this._service.clicked();
	}

	public logout() {
		this._service.logout();
	}
}