import { Inject } from "@angular/core";
import { Store, select } from '@ngrx/store';
import { IAppState } from '../app.interfaces';
import { combineLatest, of, Observable } from 'rxjs';
import { SetRequestAction } from './sub-module.actions';
import { delay, tap } from 'rxjs/operators';
import { LogoutAction } from '../base.actions';

@Inject({})
export class SubModuleService {
	private propA$ = this._store.pipe(
		select(state => state.subModule.propA)
	)
	private propB$ = this._store.pipe(
		select(state => state.subModule.propB)
	)
	public age$ = combineLatest(
		this.propA$,
		this.propB$
	).pipe(
		select(([name, age]) => {
			return `${name} is ${age} years old`
		})
	)

	constructor(private _store: Store<IAppState>) {
	
	}

	public clicked() {
		this._store.dispatch(new SetRequestAction('Value A'));
	}

	public fetchData(request: string): Observable<string> {
		return of(`Our response for "${request} is this!`).pipe(
			delay(1000),
		);
	}

	public logout() {
		this._store.dispatch(new LogoutAction());
	}
}