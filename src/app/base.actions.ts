import { Action } from '@ngrx/store';

export abstract class BaseAction<T> implements Action {
	public abstract type: string;

	constructor(public payload?: T) {}
}

export class LogoutAction extends BaseAction<never> {
	public type = 'LOGOUT';
}