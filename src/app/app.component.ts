import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from './app.interfaces';
import { SetPropAAction } from './sub-module/sub-module.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ngrx-series';
}
